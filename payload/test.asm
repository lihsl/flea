%macro enter 0
push ebp
mov ebp, esp
%endmacro

section .text
	extern printf
	global main
	str: db "helloworld",10,0
main:
	enter
	call str
	push str
	call printf
	leave
	ret
	
