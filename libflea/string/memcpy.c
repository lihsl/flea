#include "assert.h"
#include "stddef.h"
void *memcpy(unsigned char* dst, unsigned char* src, size_t s) {
	typedef struct { unsigned char dummy [32]; } dt_t;
	unsigned char* sa = (unsigned char*)src+s;
	assert(dst && src);
	dt_t *d1 = (dt_t*)dst - 1;
	dt_t *s1 = (dt_t*)src - 1;
	size_t si = s / sizeof(dt_t);

	si = (si + 7) / 8;
	switch(si % 8) 
	{
		case 0: do { *++d1 = *++s1;
		case 7:      *++d1 = *++s1;
		case 6:      *++d1 = *++s1;
		case 5:      *++d1 = *++s1;
		case 4:      *++d1 = *++s1;
		case 3:      *++d1 = *++s1;
		case 2:      *++d1 = *++s1;
		case 1:      *++d1 = *++s1;
				} while(--si > 0);
	}

	dst = (unsigned char*)d1;
	src = (unsigned char*)s1;
	while(src < sa) 
	{
		*++dst = *++src;
	}
	return dst;
}
