#include "include/stdarg.h"
#include <stdio.h>
int sum(int n1, ...)
{
	va_list arg_ptr;
	int nsum = 0, n = n1;

	va_start(arg_ptr, n1);
	while (n > 0)
	{
		nsum += va_arg(arg_ptr, int);
		n--;
	}
	va_end(arg_ptr);

return nsum;
}

int main() {
	printf("%d\n", sum(3,1,2,3));
	return 0;
}
