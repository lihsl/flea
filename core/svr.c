#include "def.h"
#include "queue.h"
#include "core.h"
#include <stdio.h>
#include <stddef.h>
#include <time.h>
#include <assert.h>


#define dword_t unsigned int

#pragma pack(push,1)
typedef struct{
	dword_t srvid; 
	dword_t msgid;
	dword_t len;
	unsigned char buf[0x1000];
} iobuf_t;
#pragma pack(pop)



typedef struct {
	int 		srvid;
	flea_entry_proc proc;
	void*		mem;
	int			timeout;
	int			timepas;
} fleaservice_t;

typedef struct { //回复类型的事件
	int srvid;
	int msgid;
} reply_t;

QUEUE_DECLARE(replyque_t, reply_t, 0x100);

typedef struct {
	fleaservice_t 	srvvect[0x100];
	flea_entry_param_t param;
	replyque_t		replyque;
	iobuf_t			inbuf;
	iobuf_t			outbuf;
	int				cursrv;
	int				srvcount;
	int				sock;
	int				recvlen;
	int				sendlen;
} flea_context_t;

flea_context_t __context_block;

#define ENTRY_PARAM(_msgid, _mem, _arg1, _arg2) {\
	flea_entry_param_t *__param = &(__context()->param);\
	__param->msgid = (_msgid);\
	__param->mem = (_mem);\
	__param->arg1 = (_arg1);\
	__param->arg2 = (_arg2);\
}



static int 			_service_idx(int srvid);
static unsigned long __tickcount();
static void 		__do_proc(flea_entry_proc, int);
static flea_context_t *__context();


/*		--------
 */
void __hexprint(unsigned char *p, int size) {
	int i;
	printf("%%(%d) ", size);
	for(i=0;i<size;i++) {
		printf("%02hhX ", p[i]);
	}   
	putchar('\n');
}

static int _service_idx(int srvid) {
	int i,j;
	flea_context_t *context;
	
	context = __context();
	for(i=0,j=0; j<context->srvcount; i++) {
		if(context->srvvect[i].srvid != 0) {
			if(context->srvvect[i].srvid == srvid)
				return i;
			j++;
		}
	}
	return -1;
}

static unsigned long __tickcount() {
	struct timespec ts;  
	clock_gettime(CLOCK_MONOTONIC, &ts);  
	printf("clock_gettime() ret %ld\n", (ts.tv_sec * 1000L + ts.tv_nsec / 1000000));
	return (ts.tv_sec * 1000L + ts.tv_nsec / 1000000);  
}

static int __checktimer(int pass) {
	int i,j;
	int minleft = -1;
	flea_context_t *context;
	
	context = __context();
	printf("pass = %d\n", pass);
	//if(__cursrv == 0)
	//	return -2;
	for(i=0; j<context->srvcount; i++) { //遍历模块
		if(context->srvvect[i].srvid != 0) {
			if(context->srvvect[i].timeout != -1) { //如果当前模块启用了timer
				context->srvvect[i].timepas += pass; //经过时间累加
				if(context->srvvect[i].timepas >= context->srvvect[i].timeout) {
					ENTRY_PARAM(FM_TIMER, context->srvvect[i].mem, 0, 0);
					__do_proc(context->srvvect[i].proc, i);
					context->srvvect[i].timepas = 0;
					if(minleft == -1 || minleft > context->srvvect[i].timeout) {
						minleft = context->srvvect[i].timeout;
					}
				} else {
					int left = context->srvvect[i].timeout - context->srvvect[i].timepas;
					if(minleft == -1 || minleft > left)
						minleft = left;
				}
			}
			j++;
		}
	}
	return minleft;
}

static void __do_proc(flea_entry_proc proc, int srvidx) {
	reply_t rep;
	flea_context_t *context;
	int ret,msgid;
	
	context = __context();

	msgid = context->param.msgid;
	context->cursrv = srvidx;
	ret = proc(&context->param);
	context->cursrv = 0;//Handler执行完毕后，将当前模块ID置0,避免在Handler外部调用Timer相关函数

	switch(msgid) {
		case FM_UNLOAD:
			break;
		case FM_SNDRDY:
			switch(ret) {
				case -1:
					return;
				default:
					context->outbuf.len = context->param.arg1;
					context->outbuf.msgid = context->param.msgid;
					//__outbuf.srvid = srvid;
					context->sendlen = 0;
			}
		case FM_LOAD:
		default:
			switch(ret) {
				case 0: 
					break;
				case 1:
					rep.msgid = context->param.msgid;
					rep.srvid = context->srvvect[srvidx].srvid;
					printf("srvidx = %d\n", srvidx);
					printf("QUEUE_LEN = %ld\n", QUEUE_LEN(context->replyque));
					printf("context->do_proc->rep %d %d\n", rep.srvid, rep.msgid);
					QUEUE_EN(context->replyque, rep);// Must available
					break;
			}
	}
}

static flea_context_t *__context() {
	return &__context_block;
}

int flea_init() {
	int i;
	flea_context_t *context;


	context = __context();

	context->sock 		= socket(AF_INET, SOCK_STREAM, 0);
	if(context->sock == -1)
		return -1;
	context->recvlen 	= -1;
	context->sendlen 	= -1;
	context->srvcount	= 0;
	QUEUE_CLR(context->replyque);
}


int flea_open() {
    struct sockaddr_in addr;
	int flags;
	flea_context_t *context;

	context = __context();

	bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(6666);
    inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr);

	if (connect(context->sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        return -1;
    }
	flags = fcntl(context->sock, F_GETFL, 0); 
	fcntl(context->sock, F_SETFL, flags | O_NONBLOCK);
	return 0;
}

int flea_close() {
	//TODO:
}

int flea_service_load(int srvid, flea_entry_proc proc) {
	int i,j,k = -1;
	flea_context_t *context;
	
	context = __context();
	printf("flea_service_load()\n");

	if(srvid <= 0) return -1;
	if(context->srvcount >= sizeof(context->srvvect)/sizeof(context->srvvect[0])) return -1;

	for(i=0,j=0; j<context->srvcount; i++) {
		if(context->srvvect[i].srvid == 0)
			k = i;
		else {
			if(context->srvvect[i].srvid == srvid)
				return -1;//Module already exists
			j++;
		}
	}
	puts("m4");
	if(k == -1) k = i;
	printf("k = %d\n", k);

	context->srvvect[k].srvid = srvid;
	context->srvvect[k].proc = proc;
	context->srvvect[k].timeout = -1;
	context->srvvect[k].timepas = 0;
	context->srvcount ++;
	//__cursrv = k;
	//context->cursrv = k;///new
	ENTRY_PARAM(FM_LOAD, &context->srvvect[k].mem, 0L, 0L);
	__do_proc(proc, k);
	printf("loadservice count = %d\n", context->srvcount);
	return 0;
}


int flea_service_unload(int srvid) {
	int idx;
	flea_context_t *context;
	
	context = __context();
	idx = _service_idx(srvid);
	if(idx != -1) {
		//__cursrv = idx;
		ENTRY_PARAM(FM_UNLOAD, &context->srvvect[idx].mem, 0L, 0L);
		context->srvvect[idx].proc(&context->param);
		// call "handlefinish"
		context->srvvect[idx].srvid = 0;
		__do_proc(context->srvvect[idx].proc, idx);
		return 0;
	}
	return -1;//Module 'srvid' not found

}

void flea_timer_start(int msec) {
	flea_context_t *context;
	context = __context();
	printf("cursrv = %d\n", context->cursrv);
	//assert(context->cursrv != 0); //new
	context->srvvect[context->cursrv].timeout = msec;
	
}

void flea_timer_stop() {
	//assert(__cursrv != -1);
	flea_context_t *context;
	
	context = __context();
	context->srvvect[context->cursrv].timeout = -1;
}

int flea_run() {
	struct timeval* ptimeout, timeout={0,0};
	flea_context_t *context;
	fd_set infds, outfds, *pinfds, *poutfds;

	context = __context();
	ptimeout = &timeout;
	for(;;) {
		int maxfd,ret;
		unsigned long tick;
		printf("[*] LOOP\n");

		tick = __tickcount();
		FD_ZERO(&infds); 
		FD_SET(context->sock, &infds);

		FD_ZERO(&outfds); 
		FD_SET(context->sock, &outfds);

		maxfd = context->sock+1;

		pinfds = (QUEUE_LEN(context->replyque) == QUEUE_SIZE(context->replyque)) ? NULL : &infds;
		poutfds = QUEUE_ISEMPTY(context->replyque) ? NULL : &outfds;

		if(ptimeout) {
			printf("ptimeout->(sec %lu usec %lu)\n", (unsigned long)ptimeout->tv_sec, (unsigned long)ptimeout->tv_usec);
		}
		printf("select %p %p %p\n", pinfds, poutfds, ptimeout);
		ret = select(maxfd, pinfds, poutfds, NULL, ptimeout);
		printf("select tick: %ld\n", __tickcount() - tick);
		switch(ret){
			case -1: //select 错误
				perror("select");
				exit(-1);
				break;
			case 0: //超时
				printf("select ret 0, timeout\n");
				break;
			default:  //可读、可写、错误
				printf("select ret %d\n", ret);
				if(FD_ISSET(context->sock,&infds)) { //处理可读
#define HEADER_SIZE offsetof(iobuf_t,buf)
					int len;
					if(context->recvlen == -1) {
						len = recv(context->sock, (void*)&context->inbuf, HEADER_SIZE, MSG_PEEK); //读取头部
						if(len >= HEADER_SIZE) {
							puts("m11");
							__hexprint((void*)&context->inbuf,len);
							len = recv(context->sock, &context->inbuf, HEADER_SIZE + context->inbuf.len, 0);
							__hexprint((void*)&context->inbuf,len);
							if(len > 0) {
								context->recvlen = len;
								printf("pkg len %d %ld\n",context->recvlen, context->inbuf.len + HEADER_SIZE);
							}
						}
					} else {
						len = recv(context->sock, 
								context->inbuf.buf + (context->recvlen - HEADER_SIZE), 
								context->inbuf.len - (context->recvlen - HEADER_SIZE), 
								0);
						if(len > 0) {
							context->recvlen += len;
						}
					}

					if(len < 0) { //任意一个recv出错
							printf("len = %d\n", len);
							perror("recv");
							goto endloop;
					} else if(len == 0) {
						int i,j;
						for(i=0,j=0; j<context->srvcount; i++) {
							if(context->srvvect[i].srvid != 0) {
								//__cursrv = i;
								ENTRY_PARAM(FM_UNLOAD, context->srvvect[i].mem, 0L, 0L);
								__do_proc(context->srvvect[i].proc, i);
								j++;
							}
						}

						printf("closed\n");

						goto endloop;
					}

					if(context->recvlen - HEADER_SIZE == context->inbuf.len) {
						puts("m3");
						int idx = _service_idx(context->inbuf.srvid);
						if(idx == -1) {
							printf("Unknow srv Id: %u, package thrown away.", context->inbuf.srvid);
							continue;
						} else {
							//__cursrv = idx;
							ENTRY_PARAM((int)context->inbuf.msgid, &context->srvvect[idx].mem, (unsigned long)context->inbuf.buf, (unsigned long)context->inbuf.len);
							__do_proc(context->srvvect[idx].proc, idx);
							context->recvlen = -1;
						}
					} else {
						// Send not finished
					}
				} // end if
				if(poutfds && FD_ISSET(context->sock, &outfds)) { //处理可写
					if(context->sendlen == -1) {
						reply_t repl;
						int idx;
						QUEUE_DE(context->replyque, repl);
						printf("repl %d %d\n", repl.srvid, repl.msgid);
						idx = _service_idx(repl.srvid); //idx 一定存在
						printf("_service = %d, idx = %d\n", context->srvcount, idx);
						if(idx == -1) puts("[!]AF idx != -1"), *(int*)NULL = 0;
						//__cursrv = idx;
						ENTRY_PARAM(FM_SNDRDY, &context->srvvect[idx].mem, (unsigned long)repl.msgid, (unsigned long)context->outbuf.buf);
						__do_proc(context->srvvect[idx].proc, idx);
						
					} 
					printf("__sendlen %d\n", context->sendlen);
					if(context->sendlen != -1) {
						context->sendlen += send(context->sock,
								(void*)&context->outbuf + context->sendlen,
								(context->outbuf.len + HEADER_SIZE) - context->sendlen,
								0);
						printf("__sendlen2 %d\n", context->sendlen);
						if(context->sendlen ==  HEADER_SIZE + context->outbuf.len) {
							//over
							context->sendlen = -1;
						}
					}
					
				} // end if
		}// end switch

		//timer
		int timeout_ms = __checktimer(__tickcount() - tick);
		//printf("pass = %d\n", pass);
		//exit(-1);
		printf("__checktimer ret %d\n", timeout_ms);
		if(timeout_ms == -1)
			ptimeout = NULL;
		else {
			timeout.tv_sec = timeout_ms/ 1000;
			timeout.tv_usec = timeout_ms % 1000 * 1000;
			ptimeout = &timeout;
		}
	}// end while
endloop:;
}







