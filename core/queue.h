#ifndef QUEUE_H
#define QUEUE_H
#define QUEUE_DECLARE(_type, _nodetype, _size) \
	    typedef struct { \
			        int rear; \
			        int front; \
			        _nodetype vect[(_size) + 1]; \
			    } _type; 
#define QUEUE_CLR(_q)       (void)((_q).front = 0, (_q).rear = 0)
#define QUEUE_ISEMPTY(_q)   ((_q).front == (_q).rear)
#define QUEUE_SIZE(_q)      (sizeof((_q).vect)/sizeof((_q).vect[0]) - 1)
#define QUEUE_EN(_q, _v) (\
    ((((_q).front + 1) % (QUEUE_SIZE(_q)+1) == (_q).rear)) ? -1 \
    : (((_q).vect[(_q).front] = (_v)), \
			(_q).front = ((_q).front + 1) % (QUEUE_SIZE(_q)+1), 0))
#define QUEUE_DE(_q, _v) (\
    ((_q).front == (_q).rear) ? -1 \
    : (_v = (_q).vect[(_q).rear], ((_q).rear = ((_q).rear + 1) % (QUEUE_SIZE(_q)+1)), 0))
#define QUEUE_LEN(_q) (\
    ((_q).front < (_q).rear) \
    ? ((QUEUE_SIZE(_q)+1) - ((_q).rear - (_q).front)) \
    : (_q).front - (_q).rear)

#endif
