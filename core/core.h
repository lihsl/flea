#define FM_LOAD   0
#define FM_UNLOAD 1
#define FM_OPENED 2
#define FM_CLOSED 3
#define FM_ERROR  4
#define FM_URG    5		//Urgent data
#define FM_SNDRDY 6 	//Send ready
#define FM_TIMER  7 	//Timer
#define FM_INPUT  8
#define FM_OUTPUT 9
#define FM_USER   16

typedef struct {
	int 		msgid;
	void** 		mem;
	unsigned long arg1;
	unsigned long arg2;//void*/int
} flea_entry_param_t;


typedef int(*flea_entry_proc)(flea_entry_param_t*);

int flea_init();
int flea_destroy();
int flea_open();
int flea_close();
int flea_service_load(int srvid, flea_entry_proc proc);
int flea_service_unload(int srvid);
void flea_timer_start(int msec);
void flea_timer_stop();
int flea_run();

void __hexprint(unsigned char *p, int size);
