section .text
	global add
ch1:
	db "A"
add:
	push ebp
	mov ebp, esp
	mov eax, dword[ebp+8]
	mov edx, dword[ebp+12]
	add eax, edx
	;call ch1
	mov esp, ebp
	pop ebp
	ret
